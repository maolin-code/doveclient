package handler

import (
	"doveclient/config"
	"encoding/json"
	"fmt"
	"strings"
)

func init() {
	handlers["GetEtcdAddr"] = EtcdAddr{}
}

// EtcdAddr 获得当前etcd的地址
type EtcdAddr struct {
}

// Process 执行获取特定环境etcd服务器地址的命令
func (i EtcdAddr) Process(args map[string]interface{}) ([]byte, error) {
	// 默认获取当前环境的etcd配置
	if _, ok := args["env"]; !ok {
		args["env"] = config.GetConfig().ENV
	}

	if _, ok := args["env"].(string); !ok {
		return nil, fmt.Errorf("无效的环境参数")
	}

	data, err := config.GetEtcdAddr(args["env"].(string))
	if err != nil {
		return nil, err
	}

	return json.Marshal(strings.Split(data.Hosts, ","))
}
