package handler

import (
	"doveclient/config"
	"strings"
)

func init() {
	handlers["GetCurrentEnv"] = CurrentEnv{}
}

// CurrentEnv 获取当前环境
type CurrentEnv struct {
}

// Process 执行获取特定环境etcd服务器地址的命令
func (i CurrentEnv) Process(args map[string]interface{}) ([]byte, error) {
	return []byte(strings.ToLower(strings.ReplaceAll(config.GetConfig().ENV, "-", ""))), nil
}
