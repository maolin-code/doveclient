# DoveClient

## 构建
1. 将Docs/settings.pack.toml.example 拷贝到上层目录，并重命名为settings.pack.toml
1. 根据实际情况对settings.pack.toml中的内容进行修改。 **注意: settings.pack.toml的每次内容变动，都应当相应的增加其中的版本号Ver**
1. 进行构建: `make`    
    _doveclient被编译至 build/DoveClient_
* 可以用make help来查看其它可用的make规则

## 其它说明
1. 可通过4514端口执行相关命令. 通过help命令显示可用的命令列表。 statsz导出的gz压缩数据，保存为文件后可进行解压，还原成json字符串.

## 配置说明

```go
type Config struct {
    // 校验服务监听地址, 如果不配置则标识不启用配置校验服务, 例如:0.0.0.0:1234
    VerificationListenAddr string
    // 配置监听目录, 多个目录则使用逗号分隔
    CONFIG_DIR            []string
    // 配置更新完成后doveclient需要执行的操作, 目前仅支持ReloadPHPServer, 如无特殊需要可以配置为空.
    POST_UPDATE_CALLBACKS []string
    // 配置更新完成后需要执行的脚本文件的路径
    POST_UPDATE_SCRIPTS   []string
    // 当前生效的环境
	ENV                   string
    // 业务可以在CONFIG_DIR之外指定debug文件从而在所有环境下影响CONFIG_DIR目录下解析出来的配置值(街点的特殊需求)
	DEBUG_FILE            string

    // 该配置参数是一个toml文件的路径
    // 如果不指定schema, 那么则特定的脚本每次配置更新都会执行, 否则只有当特定的schema有更新的时候才会执行
    /**
     * 文件内容格式
     * [rules]
     * 0 = ["脚本文件路径"]
     * 1 = ["脚本文件路径", "完整的schema文件路径1"， "完整的schema文件路径2"]
     */
    POST_UPDATE_SCRIPTS_RULE string
    // 如该配置值为yes, 则会去掉 配置值 两侧的双引号; 该开关对*.schema.php/*.schema.toml/*.schema.yml类型的配置文件无效.
	UNWRAP_STRING string
	// pprof的监听接口, 例如:0.0.0.0:12000; 如果不配置则不开启pprof性能分析.
	PPROF_LISTEN string
	// 当一个ENV环境的配置未编译到dovecleint的时候, 通过配置DOVE_SERVER可以是doveclient从远程查询ENV环境对应的etcd地址, 例如: https://dove.int.jumei.com/
	DOVE_SERVER string
    // 在某些环境下无法通过/etc/init.d/phpserver reload的方式对phpserver执行reload操作, 因此这些环境的phpserver会监听一个特定的端口, 通过向这个端口发送reload命令执行phpserver的reload操作; 例如: 127.0.0.1:10101
	PHPSERVER_CONTROL_CMD_PORT string
}
```

## 版本
### 0.9.3
    1. 修正了在启动客户端时，如果特殊参数传递错误，会导致客户端循环重启的问题。
    1. 解析配置时对hashtable格式的配置，可以保持其原有的属性顺序。
    1. 添加了客户端统计数据查询服务。可通过 telnet 127.0.0.1 4515进入控制台，　执行stats 来显示统计数据，或者statsz获取压缩后的统计数据。也可以通过命令　echo "statsz\r\n" | nc 127.0.0.1 4514　> stats.gz  来导出压缩后的统计数据。
    1. 不再强制使用root用户来启动客户端，　可以在配置里设置为其它用户，但需注意，此用户必须有对业务项目配置目录及其下的文件有读写的权限。
### 0.9.4
    1. 修正了DATA_DIR不能自定义的问题。 
    1. 完全兼容windows系统。 
    1. 编译配置模板时采用更保守的方式，以确保配置能被获取到（不再仅从内存中获取）。
### 0.9.5 
    1. 优化leveldb的读写操作，不再使用段连接，提升性能(大概2000倍),同时，解决了短链接产生过多数据日志文件的问题。
    1. 优化zk连接状态监控，避免了多一次更新全部配置的操作。
### 0.9.6
    1. 优化了restart/stop过程，提高执行速度，解决几率性失败的问题。
    1. 修正了在编译配置模板时，如果上一个配置获取失败会导致下一个配置不能获取的问题。
    1. 修正了打包脚本不能跨平台编译linux平台版本的客户端问题。
    
### 0.9.7
    1. 更新zk包至最新版本
### 0.20.0
    1. 将配置存储从zookeeper迁移到etcd
    1. 支持.schema.toml文件解析
    1. 支持将有变化的配置项发送给回调脚本
    1. 支持特定的schema文件更新后调用特定的回调脚本
### 0.20.4.2-beta
    1. 添加pprof性能分析
    1. 支持特定的schema文件更新后调用特定的回调脚本
### 0.20.4.3
    1. 添加SH机房支持
### 0.20.4.4
    1. 支持jd global环境
### 0.20.5
    1. 支持动态添加环境
### 0.20.6
    1. 优先从远程服务器获取环境信息, 远程没有再检查环境信息是否预编译到二进制包
    1. 支持平滑重启(针对rpc)
### 0.20.7
    1. 升级etcd/clientv3库,解决etcd内存使用内存暴增的问题.
### 0.20.8
    1. chaos修改的版本，彻底解决内存泄露的问题.
### 0.20.8.1
    1. 支持从环境变量读取DoveClient运行配置.
### 0.20.8.2
    1. 支持直接指定DEBUG_FILE(或从DOVE_DEBUG_FILE环境变量获取).
### 0.20.9
    1. chaos修复了移除etcd节点时doveclient内存溢出的问题.
### 0.20.10
    1. 修复statistic服务端口被非法访问时，可能会引起进程崩溃的问题.
    1. 修复使用错误的日志级别导致进程崩溃的问题.
### 0.20.11
    1. 修复非法日志级别导致的panic
    2. 启动时，如果修改目录权限失败，不退出进程。
    
### 0.20.12
    1. 调整心跳上报间隔，减少上报次数。
    2. 不上报更新结果，减少etcd读写压力。
### 0.20.13
    1. 修复日志参数缺失 
    2. 支持通过配置文件以及环境变量设置reload phpserver的ip和端口(PHPSERVER_CONTROL_CMD_PORT)
### 0.20.14
    1. 增加VerificationListenAddr配置
    2. 支持远程获取目标配置文件的校验值
    3. 支持远程获取shema文件列表，schama文件中的配置项 以及 对应配置项的值
### 0.20.15
    1. 支持.schema.yaml格式解析
    2. 支持.schema. *后缀的所有模板文件(只执行简单的配置替换)

### 0.20.16
    1. 支持unWrapString模式(DoveClient.conf UNWRAP_STRING="yes"即可开启), 该模式下编译出的字符串类型的配置值会去掉两侧的引号.

### 0.20.17 & 0.20.17.1
    1. 修复statistics, configwatcher中对map的访问存在数据竞争的问题

### 0.21.0
    1. 用go mod进行依赖管理; 
    2. 修复etcd断线后,会造成100%cpu占用的问题 
    
### 0.21.0
    1. 修正部分文档
    2. 支持控制台打印readme文档

### 0.21.1
    1. 支持通过api查询特定环境的etcd服务器列表
